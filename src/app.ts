import express from 'express'

import apiMtbBeskydyRoute from './api/v1/index'

const app = express()
const port = 3000

app.disable('view cache')

app.use((req, res, next) => {
  next()
})

app.use('/api/v1/mtb-beskydy', apiMtbBeskydyRoute)

app.listen(port, () => {
  console.log(`Timezones by location application is running on port ${port}.`)
})

module.exports = app
