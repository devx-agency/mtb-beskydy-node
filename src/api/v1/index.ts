import express from 'express'
import { pathOr } from 'ramda'
import path from 'path'
import routes from '../../../data/routes.json'
import routesText from '../../../data/routesText.json'

const router = express.Router()

router.get('/', (req, res) => {
  const presentationId = req.headers['accept-language'] === 'cs-CZ' ? 1 : 2
  const response = routes.map((route) => {
    const data = routesText.find(
      (item) =>
        item.routeId === route.id && item.presentationId === presentationId,
    )

    return {
      id: route.id,
      routeId: data.routeId,
      imageUrl: route.imageUrl,
      routeUrl: data.routeUrl,
      distance: route.distance,
      maxAltitude: route.maxAltitude,
      minAltitude: route.minAltitude,
      elevationUp: route.elevationUp,
      elevationDown: route.elevationDown,
      level: route.level,
      textId: data.id,
      name: data.name,
      description: data.description,
      presentationId: data.presentationId,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
    }
  })

  res.json(response)
})

router.get('/gpx/:id', (req, res) => {
  const response = routes.find(
    (item) => Number(item.id) === parseInt(req.params.id),
  )
  res.json({
    id: pathOr('', ['id'], response),
    gpx: pathOr('', ['gpx'], response),
  })
})

router.get('/image/:image', (req, res) => {
  res.sendFile(
    path.resolve(path.join('./', 'images/', req.params.image)),
    (error) => {
      if (error) {
        res.status(404).json({
          message: 'NOT_FOUND',
        })
      }
    },
  )
})

export default router
