FROM node:14-alpine AS stage0

WORKDIR /workspace

COPY ./ ./

RUN yarn install --production=true
RUN yarn build

FROM node:14-alpine

WORKDIR /workspace

COPY --from=stage0 ./workspace .

EXPOSE 3000
CMD ["yarn", "start"]
